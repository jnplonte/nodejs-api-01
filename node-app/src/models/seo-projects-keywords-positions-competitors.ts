import { Sequelize } from 'sequelize';

export interface SeoProjectsKeywordsPositionsCompetitorsAttributes {
	project_id: number;
	keyword_id?: number;
	competitor_id?: number;
	position_mobile?: number;
	position_desktop?: number;
	position_date?: Date;
}

export default function (sequelize: Sequelize, dataTypes: any) {
	const seo_projects_keywords_positions_competitors = sequelize.define(
		'seo_projects_keywords_positions_competitors',
		{
			keyword_id: {
				type: dataTypes.INTEGER,
				primaryKey: true,
			},
			project_id: {
				type: dataTypes.INTEGER,
			},
			competitor_id: {
				type: dataTypes.INTEGER,
			},
			position_mobile: {
				type: dataTypes.INTEGER,
			},
			position_desktop: {
				type: dataTypes.INTEGER,
			},
			position_date: {
				type: dataTypes.DATE,
			},
		},
		{
			freezeTableName: true,
			timestamps: false,
		}
	);

	seo_projects_keywords_positions_competitors['associate'] = function (models) {
		models.seo_projects_keywords_positions_competitors.belongsTo(models.seo_projects_keywords, {
			foreignKey: 'keyword_id',
		});

		models.seo_projects_keywords_positions_competitors.belongsTo(models.seo_projects_competitors, {
			foreignKey: 'competitor_id',
		});
	};

	seo_projects_keywords_positions_competitors.removeAttribute('id');

	return seo_projects_keywords_positions_competitors;
}
