import { Sequelize } from 'sequelize';

export interface AccountsAttributes {
	account_id: number;
	account_name?: string;
	account_vat?: string;
	account_avatar?: string;
	account_registered?: Date;
	account_country?: string;
	account_stripe_id?: string;
	account_stripe_subscription_id?: string;
	account_stripe_credit_card_id?: string;
	account_trial_end_date?: number;
	account_stripe_vat_id?: string;
	account_subscription_status?: string;
	account_subscription_expire_date?: number;
	account_email?: string;
	account_currency?: string;
	actapi_keyive?: string;
}

export default function (sequelize: Sequelize, dataTypes: any) {
	const accounts = sequelize.define(
		'accounts',
		{
			account_id: {
				type: dataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
			account_name: {
				type: dataTypes.STRING,
			},
			account_vat: {
				type: dataTypes.STRING,
			},
			account_avatar: {
				type: dataTypes.STRING,
			},
			account_registered: {
				type: dataTypes.DATE,
			},
			account_country: {
				type: dataTypes.STRING,
			},
			account_stripe_id: {
				type: dataTypes.STRING,
			},
			account_stripe_subscription_id: {
				type: dataTypes.STRING,
			},
			account_stripe_credit_card_id: {
				type: dataTypes.STRING,
			},
			account_trial_end_date: {
				type: dataTypes.INTEGER,
			},
			account_stripe_vat_id: {
				type: dataTypes.STRING,
			},
			account_subscription_status: {
				type: dataTypes.STRING,
			},
			account_subscription_expire_date: {
				type: dataTypes.INTEGER,
			},
			account_email: {
				type: dataTypes.STRING,
			},
			account_currency: {
				type: dataTypes.STRING,
			},
			api_key: {
				type: dataTypes.STRING,
			},
		},
		{
			freezeTableName: true,
			timestamps: false,
		}
	);

	accounts.removeAttribute('id');

	return accounts;
}
