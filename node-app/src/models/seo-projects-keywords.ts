import { Sequelize } from 'sequelize';

export interface SeoProjectsCompetitorsAttributes {
	project_id: number;
	keyword_id?: number;
	se_id?: number;
	loc_id?: number;
	keyword?: string;
	deleted?: boolean;
}

export default function (sequelize: Sequelize, dataTypes: any) {
	const seo_projects_keywords = sequelize.define(
		'seo_projects_keywords',
		{
			keyword_id: {
				type: dataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
			project_id: {
				type: dataTypes.INTEGER,
			},
			se_id: {
				type: dataTypes.INTEGER,
			},
			loc_id: {
				type: dataTypes.INTEGER,
			},
			keyword: {
				type: dataTypes.STRING,
			},
			deleted: {
				type: dataTypes.BOOLEAN,
			},
		},
		{
			freezeTableName: true,
			timestamps: false,
		}
	);

	seo_projects_keywords['associate'] = function (models) {
		models.seo_projects_keywords.hasMany(models.seo_projects_keywords_positions, {
			foreignKey: 'keyword_id',
			sourceKey: 'keyword_id',
		});

		models.seo_projects_keywords.hasMany(models.seo_projects_keywords_positions_competitors, {
			foreignKey: 'keyword_id',
			sourceKey: 'keyword_id',
		});
	};

	seo_projects_keywords.removeAttribute('id');

	return seo_projects_keywords;
}
