import { Sequelize } from 'sequelize';

export interface SeoProjectsCompetitorsAttributes {
	competitor_id: number;
	project_id?: number;
	competitor_name?: string;
	competitor_url?: string;
	deleted?: boolean;
}

export default function (sequelize: Sequelize, dataTypes: any) {
	const seo_projects_competitors = sequelize.define(
		'seo_projects_competitors',
		{
			competitor_id: {
				type: dataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
			project_id: {
				type: dataTypes.INTEGER,
			},
			competitor_name: {
				type: dataTypes.STRING,
			},
			competitor_url: {
				type: dataTypes.STRING,
			},
			deleted: {
				type: dataTypes.BOOLEAN,
			},
		},
		{
			freezeTableName: true,
			timestamps: false,
		}
	);

	seo_projects_competitors.removeAttribute('id');

	seo_projects_competitors['associate'] = function (models) {
		models.seo_projects_competitors.belongsTo(models.seo_projects_keywords_positions_competitors, {
			foreignKey: 'competitor_id',
		});
	};

	return seo_projects_competitors;
}
