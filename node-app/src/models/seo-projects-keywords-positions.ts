import { Sequelize } from 'sequelize';

export interface SeoProjectsKeywordsPositionsAttributes {
	project_id: number;
	keyword_id?: number;
	position_mobile?: number;
	position_desktop?: number;
	position_date?: Date;
}

export default function (sequelize: Sequelize, dataTypes: any) {
	const seo_projects_keywords_positions = sequelize.define(
		'seo_projects_keywords_positions',
		{
			keyword_id: {
				type: dataTypes.INTEGER,
				primaryKey: true,
			},
			project_id: {
				type: dataTypes.INTEGER,
			},
			position_mobile: {
				type: dataTypes.INTEGER,
			},
			position_desktop: {
				type: dataTypes.INTEGER,
			},
			position_date: {
				type: dataTypes.DATE,
			},
		},
		{
			freezeTableName: true,
			timestamps: false,
		}
	);

	seo_projects_keywords_positions['associate'] = function (models) {
		models.seo_projects_keywords_positions.belongsTo(models.seo_projects_keywords, {
			foreignKey: 'keyword_id',
		});
	};

	seo_projects_keywords_positions.removeAttribute('id');

	return seo_projects_keywords_positions;
}
