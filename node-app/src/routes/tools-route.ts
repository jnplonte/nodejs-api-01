import { Helper } from '../app/services/helper/helper.service';
import { ApiResponse } from '../app/services/api-response/api-response.service';
import { Query } from '../app/services/query/query.service';

import { SeoAudit } from '../app/v1/tools/seo-audit/seo-audit.component';

export function setup(app, cache, config, models) {
	const response = new ApiResponse(),
		helper = new Helper(config),
		query = new Query(config);

	const verifyUser = (req, res, next) => {
		res.startTime = new Date().getTime();

		if (typeof req.headers === 'undefined' || helper.isEmpty(req.headers[config.secretKey])) {
			return response.failed(res, 'token', '', 401);
		}

		query
			.getOne(models.accounts, {}, 'api_key', req.headers[config.secretKey])
			.then((account) => {
				if (typeof account === 'undefined' || helper.isEmptyObject(account)) {
					return response.failed(res, 'token', '', 401);
				}

				res.express_redis_cache_name = `${req.originalUrl}:`;
				req.models = models;

				if (!req.models) {
					return response.failed(res, 'model', '', 500);
				}

				next();
			})
			.catch((error) => {
				return response.failed(res, 'token', '', 401);
			});
	};

	app.version('v1/tools', (appCore) => {
		appCore.use(verifyUser);

		new SeoAudit(appCore, cache, response, helper);
	});

	return app;
}
