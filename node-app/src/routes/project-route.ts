import { Helper } from '../app/services/helper/helper.service';
import { ApiResponse } from '../app/services/api-response/api-response.service';
import { Query } from '../app/services/query/query.service';

import { SeoProjectsKeywords } from '../app/v1/projects/seo-projects-keywords/seo-projects-keywords.component';
import { SeoProjectsCompetitors } from '../app/v1/projects/seo-projects-competitors/seo-projects-competitors.component';

export function setup(app, cache, config, models) {
	const response = new ApiResponse(),
		helper = new Helper(config),
		query = new Query(config);

	const verifyUser = (req, res, next) => {
		res.startTime = new Date().getTime();

		if (typeof req.headers === 'undefined' || helper.isEmpty(req.headers[config.secretKey])) {
			return response.failed(res, 'token', '', 401);
		}

		query
			.getOne(models.accounts, {}, 'api_key', req.headers[config.secretKey])
			.then((account) => {
				if (typeof account === 'undefined' || helper.isEmptyObject(account)) {
					return response.failed(res, 'token', '', 403);
				}

				res.express_redis_cache_name = `${req.originalUrl}:`;
				req.models = models;

				if (!req.models) {
					return response.failed(res, 'model', '', 500);
				}

				next();
			})
			.catch((error) => {
				return response.failed(res, 'token', '', 403);
			});
	};

	app.version('v1/project', (appCore) => {
		appCore.use(verifyUser);

		new SeoProjectsKeywords(appCore, cache, response, helper, query);
		new SeoProjectsCompetitors(appCore, cache, response, helper, query);
	});

	return app;
}
