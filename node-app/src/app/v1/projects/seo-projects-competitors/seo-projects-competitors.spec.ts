import * as supertest from 'supertest';
import { expect } from 'chai';

import app from '../../../../app';

const authKey: string = 'ofiujew89r89uj4rewu0fuse90few9f0weif9iwef90i';

describe('competitor component', () => {
	it('should get all competitor', (done) => {
		supertest(app)
			.get('/v1/project/competitor/list?project_id=1&test=true')
			.set('x-node-api-key', authKey)
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status_code).to.equal(200);

				done();
			});
	});

	it('should create competitor', (done) => {
		supertest(app)
			.post('/v1/project/competitor/add?test=true')
			.set('x-node-api-key', authKey)
			.send({
				project_id: '1',
				competitor_name: 'test',
				competitor_url: 'https://www.test.com',
			})
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status_code).to.equal(200);

				done();
			});
	});

	it('should update competitor', (done) => {
		supertest(app)
			.patch(`/v1/project/competitor/update??test=true`)
			.set('x-node-api-key', authKey)
			.send({
				competitor_id: '5',
				competitor_name: 'test update',
			})
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status_code).to.equal(200);

				done();
			});
	});

	it('should delete competitor', (done) => {
		supertest(app)
			.delete(`/v1/project/competitor/delete?test=true`)
			.set('x-node-api-key', authKey)
			.send({
				competitor_id: '5',
			})
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status_code).to.equal(200);

				done();
			});
	});
});
