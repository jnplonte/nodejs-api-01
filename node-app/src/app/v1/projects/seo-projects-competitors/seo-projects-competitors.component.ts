import { Request, Response } from 'express';
import { CoreMiddleware } from '../../../middlewares/core/core.middleware';

import { SeoProjectsCompetitorsAttributes } from '../../../../models/seo-projects-competitors';

export class SeoProjectsCompetitors extends CoreMiddleware {
	pageCache: any;
	nameCache: string = '/v1/project/competitor*';

	constructor(app, cache, private response, private helper, private query) {
		super(app, cache);
		this.pageCache = cache;
	}

	get services() {
		return {
			'GET /competitor/list': 'get',
			'POST /competitor/add': 'post',
			'PATCH /competitor/update': 'patch',
			'DELETE /competitor/delete': 'delete',
		};
	}

	/**
	 * @api {get} /project/competitor/list get competitor
	 * @apiVersion 1.0.0
	 * @apiName get
	 * @apiGroup SEOPROJECTSCOMPETITORS
	 * @apiPermission authenticated-user
	 *
	 * @apiDescription get competitor
	 *
	 * @apiParam (query) {Number} project_id project id
	 * @apiParam (query) {Number} [limit=10] data limit <br/>Ex. ?limit=1
	 * @apiParam (query) {Number} [page=1] page number <br/>Ex. ?page=1
	 */
	get(req: Request, res: Response): void {
		const reqParameters = ['project_id'];
		if (!this.helper.validateData(req.query, reqParameters)) {
			return this.response.failed(res, 'data', reqParameters);
		}

		const id = req.query.project_id;
		const whereData = {
			where: {
				project_id: Number(id || null),
				deleted: false,
			},
		};

		return this.query
			.getAll(req.models.seo_projects_competitors, whereData, req.query)
			.then((seoProjectCompetitors: any) => {
				const { data, pagination } = seoProjectCompetitors || { data: [], pagination: {} };
				return this.response.success(res, 'get', data || [], pagination);
			})
			.catch((error) => this.response.failed(res, 'get', error));
	}

	/**
	 * @api {post} /competitor/add insert competitor
	 * @apiVersion 1.0.0
	 * @apiName post
	 * @apiGroup SEOPROJECTSCOMPETITORS
	 * @apiPermission authenticated-user
	 *
	 * @apiDescription insert competitor
	 *
	 * @apiParam (body) {Number} project_id project id
	 * @apiParam (body) {String} competitor_name name
	 * @apiParam (body) {String} competitor_url url
	 */
	post(req: Request, res: Response): void {
		const reqParameters = ['project_id', 'competitor_name', 'competitor_url'];
		if (!this.helper.validateData(req.body, reqParameters)) {
			return this.response.failed(res, 'data', reqParameters);
		}

		const data = req.body;

		return this.query
			.post(req.models.seo_projects_competitors, data)
			.then((seoProjectCompetitor: SeoProjectsCompetitorsAttributes) => {
				const cleanSeoProjectCompetitor = seoProjectCompetitor
					? this.helper.cleanSequelizeData(seoProjectCompetitor)
					: {};

				return this.cacheCheck(
					this.pageCache,
					this.nameCache
				)(
					seoProjectCompetitor
						? this.response.success(res, 'post', cleanSeoProjectCompetitor)
						: this.response.failed(res, 'post')
				);
			})
			.catch((error) => this.response.failed(res, 'post', error));
	}

	/**
	 * @api {patch} /competitor/update update competitor
	 * @apiVersion 1.0.0
	 * @apiName patch
	 * @apiGroup SEOPROJECTSCOMPETITORS
	 * @apiPermission authenticated-user
	 *
	 * @apiDescription update competitor
	 *
	 * @apiParam (body) {Number} competitor_id competitor id
	 * @apiParam (body) {String} [competitor_name] name
	 * @apiParam (body) {String} [competitor_url] url
	 */
	patch(req: Request, res: Response): void {
		const reqParameters = ['competitor_id'];
		if (!this.helper.validateData(req.body, reqParameters)) {
			return this.response.failed(res, 'data', reqParameters);
		}

		const id = req.body.competitor_id;
		const whereData = {
			where: {
				competitor_id: Number(id || null),
			},
		};

		const data = req.body;

		return this.query
			.putGet(req.models.seo_projects_competitors, data, whereData)
			.then((seoProjectCompetitor: SeoProjectsCompetitorsAttributes) => {
				const cleanSeoProjectCompetitor = seoProjectCompetitor
					? this.helper.cleanSequelizeData(seoProjectCompetitor)
					: {};

				return this.cacheCheck(
					this.pageCache,
					this.nameCache
				)(
					seoProjectCompetitor
						? this.response.success(res, 'patch', cleanSeoProjectCompetitor)
						: this.response.failed(res, 'patch')
				);
			})
			.catch((error) => this.response.failed(res, 'patch', error));
	}

	/**
	 * @api {delete} /competitor/delete delete competitor
	 * @apiVersion 1.0.0
	 * @apiName delete
	 * @apiGroup SEOPROJECTSCOMPETITORS
	 * @apiPermission authenticated-user
	 *
	 * @apiDescription delete competitor
	 *
	 * @apiParam (body) {Number} competitor_id competitor id
	 */
	delete(req: Request, res: Response): void {
		const reqParameters = ['competitor_id'];
		if (!this.helper.validateData(req.body, reqParameters)) {
			return this.response.failed(res, 'data', reqParameters);
		}

		const id = req.body.competitor_id;
		const whereData = {
			where: {
				competitor_id: Number(id || null),
			},
		};

		const data = { deleted: true };

		return this.query
			.putGet(req.models.seo_projects_competitors, data, whereData)
			.then((seoProjectCompetitor: SeoProjectsCompetitorsAttributes) => {
				const cleanSeoProjectCompetitor = seoProjectCompetitor
					? this.helper.cleanSequelizeData(seoProjectCompetitor)
					: {};

				return this.cacheCheck(
					this.pageCache,
					this.nameCache
				)(
					seoProjectCompetitor
						? this.response.success(res, 'delete', cleanSeoProjectCompetitor)
						: this.response.failed(res, 'delete')
				);
			})
			.catch((error) => this.response.failed(res, 'delete', error));
	}
}
