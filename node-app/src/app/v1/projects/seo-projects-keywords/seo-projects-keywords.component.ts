import { Request, Response } from 'express';
import { Op } from 'sequelize';
import { CoreMiddleware } from '../../../middlewares/core/core.middleware';

import { SeoProjectsCompetitorsAttributes } from '../../../../models/seo-projects-competitors';
import { SeoProjectsKeywordsPositionsCompetitorsAttributes } from '../../../../models/seo-projects-keywords-positions-competitors';

export class SeoProjectsKeywords extends CoreMiddleware {
	pageCache: any;
	nameCache: string = '/v1/project/keyword*';

	constructor(app, cache, private response, private helper, private query) {
		super(app, cache);
		this.pageCache = cache;
	}

	get services() {
		return {
			'GET /seo/keyword/list': 'getList',
			'GET /seo/keyword/position': 'getPosition',
		};
	}

	/**
	 * @api {get} /project/seo/keyword/list get keyword
	 * @apiVersion 1.0.0
	 * @apiName getList
	 * @apiGroup SEOPROJECTSKEYWORDS
	 * @apiPermission authenticated-user
	 *
	 * @apiDescription get keyword
	 *
	 * @apiParam (query) {Number} project_id project id
	 * @apiParam (query) {Number} [limit=10] data limit <br/>Ex. ?limit=1
	 * @apiParam (query) {Number} [page=1] page number <br/>Ex. ?page=1
	 */
	getList(req: Request, res: Response): void {
		const reqParameters = ['project_id'];
		if (!this.helper.validateData(req.query, reqParameters)) {
			return this.response.failed(res, 'data', reqParameters);
		}

		const id = req.query.project_id;
		const whereData = {
			where: {
				project_id: Number(id || null),
				deleted: false,
			},
		};

		return this.query
			.getAll(req.models.seo_projects_keywords, whereData, req.query)
			.then((seoProjectKeywords: any) => {
				const { data, pagination } = seoProjectKeywords || { data: [], pagination: {} };
				return this.response.success(res, 'get', data || [], pagination);
			})
			.catch((error) => this.response.failed(res, 'get', error));
	}

	/**
	 * @api {get} /project/seo/keyword/position get position
	 * @apiVersion 1.0.0
	 * @apiName getPosition
	 * @apiGroup SEOPROJECTSKEYWORDS
	 * @apiPermission authenticated-user
	 *
	 * @apiDescription get position
	 *
	 * @apiParam (query) {Number} project_id project id
	 * @apiParam (query) {Number} [keyword_id] keyword id
	 * @apiParam (query) {Boolean} [competitor] competitor <br/>Ex. ?competitor=true|false
	 * @apiParam (query) {Number} [limit=10] data limit <br/>Ex. ?limit=1
	 * @apiParam (query) {Number} [page=1] page number <br/>Ex. ?page=1
	 */
	getPosition(req: Request, res: Response): void {
		const reqParameters = ['project_id'];
		if (!this.helper.validateData(req.query, reqParameters)) {
			return this.response.failed(res, 'data', reqParameters);
		}

		const id = req.query.project_id;
		const whereData = {
			where: {
				project_id: Number(id || null),
			},
		};

		if (req.query.keyword_id) {
			whereData['where']['keyword_id'] = Number(req.query.keyword_id || null);
		}

		let finalData: Array<any> = [];
		let finalPagination: object = {};
		return this.query
			.getAll(req.models.seo_projects_keywords, whereData, req.query)
			.then((seoProjectPositions: any) => {
				const { data, pagination } = seoProjectPositions || { data: [], pagination: {} };
				const cleanSeoProjectPositions: SeoProjectsCompetitorsAttributes[] = this.helper.cleanSequelizeDataArray(data);

				const startDate = req.query.date_from || this.helper.subtractDate(30);
				const endDate = req.query.date_to || this.helper.currentDate();

				finalData = cleanSeoProjectPositions.map((sppData: SeoProjectsCompetitorsAttributes) => {
					return { ...sppData, position: [], competitor: [] };
				});
				finalPagination = pagination;

				const promises: Array<Promise<any>> = [];
				finalData.forEach((sppData: SeoProjectsCompetitorsAttributes) => {
					promises.push(
						this.query.getAllField(req.models.seo_projects_keywords_positions, {
							where: {
								project_id: sppData['project_id'],
								keyword_id: sppData['keyword_id'],
								position_date: {
									[Op.between]: [startDate, endDate],
								},
							},
						})
					);
				});

				return Promise.all(promises);
			})
			.then((seoProjectKeywordsPositions: any) => {
				if (seoProjectKeywordsPositions.length >= 1) {
					seoProjectKeywordsPositions.forEach((spkpData, spkpIndx) => {
						finalData[spkpIndx]['position'] = this.helper.cleanSequelizeDataArray(spkpData);
					});
				}

				if (!req.query.competitor) {
					return Promise.resolve([]);
				}

				const promises: Array<Promise<any>> = [];
				finalData.forEach((sppData: SeoProjectsKeywordsPositionsCompetitorsAttributes) => {
					promises.push(
						this.query.getAllField(req.models.seo_projects_keywords_positions_competitors, {
							where: {
								project_id: sppData['project_id'],
								keyword_id: sppData['keyword_id'],
							},
							include: {
								model: req.models.seo_projects_competitors,
							},
						})
					);
				});

				return Promise.all(promises);
			})
			.then((seoProjectKeywordsPositionsCompetitors: any) => {
				if (seoProjectKeywordsPositionsCompetitors.length >= 1) {
					seoProjectKeywordsPositionsCompetitors.forEach((spkpcData, spkpcIndx) => {
						finalData[spkpcIndx]['competitor'] = this.helper.cleanSequelizeDataArray(spkpcData);
					});
				}

				return this.response.success(res, 'get', finalData || [], finalPagination);
			})
			.catch((error) => this.response.failed(res, 'get', error));
	}
}
