import * as supertest from 'supertest';
import { expect } from 'chai';

import app from '../../../../app';

const authKey: string = 'ofiujew89r89uj4rewu0fuse90few9f0weif9iwef90i';

describe('keywords component', () => {
	it('should get all keyword', (done) => {
		supertest(app)
			.get('/v1/project/seo/keyword/list?project_id=1&test=true')
			.set('x-node-api-key', authKey)
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status_code).to.equal(200);

				done();
			});
	});

	it('should get all positon', (done) => {
		supertest(app)
			.get('/v1/project/seo/keyword/position?project_id=1&test=true')
			.set('x-node-api-key', authKey)
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status_code).to.equal(200);

				done();
			});
	});
});
