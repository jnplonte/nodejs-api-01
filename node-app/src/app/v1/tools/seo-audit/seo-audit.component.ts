import * as lighthouse from 'lighthouse';
import * as chromeLauncher from 'chrome-launcher';

import { Request, Response } from 'express';
import { CoreMiddleware } from '../../../middlewares/core/core.middleware';

export class SeoAudit extends CoreMiddleware {
	pageCache: any;
	nameCache: string = '/v1/tools/seo*';

	constructor(app, cache, private response, private helper) {
		super(app, cache);
		this.pageCache = cache;
	}

	get services() {
		return {
			'GET /seoaudit': 'seoAudit',
		};
	}

	/**
	 * @api {get} /tools/seoaudit seo audit
	 * @apiVersion 1.0.0
	 * @apiName seoAudit
	 * @apiGroup SEOAUDIT
	 * @apiPermission authenticated-user
	 *
	 * @apiDescription seo audit
	 *
	 * @apiParam (query) {String} url url audit <br/>Ex. ?url=https://www.google.com
	 * @apiParam (query) {String} device device <br/>Ex. ?device=mobile|desktop
	 * @apiParam (query) {String} locale locale <br/>Ex. ?locale=EN|SV
	 * @apiParam (query) {Boolean} performance performance score <br/>Ex. ?performance=true|false
	 * @apiParam (query) {Boolean} accessibility accessibility score <br/>Ex. ?accessibility=true|false
	 * @apiParam (query) {Boolean} bestPractices best practices score <br/>Ex. ?bestPractices=true|false
	 * @apiParam (query) {Boolean} pwa progressive web app score <br/>Ex. ?pwa=true|false
	 * @apiParam (query) {Boolean} seo search engine optimization score <br/>Ex. ?seo=true|false
	 */
	async seoAudit(req: Request, res: Response) {
		const reqParameters = ['url', 'device'];
		if (!this.helper.validateData(req.query, reqParameters)) {
			return this.response.failed(res, 'data', reqParameters);
		}

		const categories: string[] = [];
		if (req.query.performance) {
			categories.push('performance');
		}

		if (req.query.accessibility) {
			categories.push('accessibility');
		}

		if (req.query.bestPractices) {
			categories.push('best-practices');
		}

		if (req.query.pwa) {
			categories.push('pwa');
		}

		if (req.query.seo) {
			categories.push('seo');
		}

		if (categories.length === 0) {
			categories.push('performance'); // performance default category
		}

		try {
			const locale: string = req.query.language ? (req.query.language as string) : 'EN';
			const chrome = await chromeLauncher.launch({
				chromeFlags: ['--headless', '--disable-gpu', '--no-sandbox'],
			});
			const options = {
				locale: locale,
				logLevel: 'info',
				output: 'json',
				onlyCategories: categories,
				skipAudits: ['full-page-screenshot'],
				port: chrome.port,
				extends: 'lighthouse:default',
			};
			const runnerResult = await lighthouse(req.query.url, options);

			await chrome.kill();

			const finalSeoAudit: object = {
				url: runnerResult.lhr.finalUrl,
				summary: runnerResult.lhr.audits,
				performance: categories.includes('performance') ? runnerResult.lhr.categories['performance'] : '',
				accessibility: categories.includes('accessibility') ? runnerResult.lhr.categories['accessibility'] : '',
				bestPractices: categories.includes('best-practices') ? runnerResult.lhr.categories['best-practices'] : '',
				pwa: categories.includes('pwa') ? runnerResult.lhr.categories['pwa'] : '',
				seo: categories.includes('seo') ? runnerResult.lhr.categories['seo'] : '',
			};

			return this.response.success(res, 'seo-audit', finalSeoAudit);
		} catch (error) {
			await chromeLauncher.killAll();

			return this.response.failed(res, 'seo-audit', error);
		}
	}
}
