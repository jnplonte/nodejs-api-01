import * as supertest from 'supertest';
import { expect } from 'chai';

import app from '../../../../app';

const authKey: string = 'ofiujew89r89uj4rewu0fuse90few9f0weif9iwef90i';

describe('seo audit component', () => {
	it('should get all seo audit', (done) => {
		supertest(app)
			.get(
				'/v1/tools/seoaudit?url=http://www.jnpl.me&device=desktop&performance=true&accessibility=true&bestPractices=true&pwa=true&seo=true&test=true'
			)
			.set('x-node-api-key', authKey)
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status_code).to.equal(200);

				done();
			});
	});
});
