import { expect } from 'chai';
import { Query } from './query.service';
import { baseConfig } from './../../../config';

import { setup } from './../../../models';

describe('query service', () => {
	let services;
	let models;

	const getfinalData = function (data) {
		data = typeof data.get !== 'undefined' ? data.get({ plain: true }) : data;

		const finalData: Object = { data: data.data || [], pagination: data.pagination || {} };

		return finalData['data'];
	};

	beforeEach((done) => {
		services = new Query(baseConfig);
		models = setup();

		done();
	});

	it('should check if models exists', (done) => {
		expect(models.accounts).to.exist;
		expect(models.accounts.findAll).to.exist;
		expect(models.accounts.findAll).to.be.a('function');
		expect(models.accounts.findOne).to.exist;
		expect(models.accounts.findOne).to.be.a('function');
		expect(models.accounts.create).to.exist;
		expect(models.accounts.create).to.be.a('function');
		expect(models.accounts.update).to.exist;
		expect(models.accounts.update).to.be.a('function');
		expect(models.accounts.destroy).to.exist;
		expect(models.accounts.destroy).to.be.a('function');

		done();
	});

	it('should mock get all data with limit', (done) => {
		services
			.getAll(models.accounts, {}, { limit: 1 })
			.then((data) => {
				expect(getfinalData(data)).to.have.lengthOf(1);

				done();
			})
			.catch(done);
	});
});
