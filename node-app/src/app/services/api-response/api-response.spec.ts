import { expect } from 'chai';
import { ApiResponse } from './api-response.service';

describe('api response service', () => {
	let services;

	beforeEach((done) => {
		services = new ApiResponse();

		done();
	});

	it('should get the error message', (done) => {
		expect(services.getError('SequelizeDatabaseError')).to.equal('Database Error');
		expect(services.getError('NetworkingError')).to.equal('Network Error');

		done();
	});

	it('should get the success payload', (done) => {
		expect(services.success({}, 'post', 'test-data').status_code).to.equal(200);
		expect(services.success({}, 'put', 'test-data', { count: 1 }).status_code).to.equal(200);
		expect(services.success({}, 'get', 'test-data', { count: 1 }).pagination).to.eql({ count: 1 });

		done();
	});
});
