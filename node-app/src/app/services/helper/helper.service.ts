import {
	toJson,
	toString,
	isEmpty,
	isNotEmpty,
	cleanData,
	isEmptyObject,
	isStringExist,
	checkObjectInList,
	isInteger,
	isInArray,
	cleanDataWithNull,
	cleanDataRemoveNull,
	pad,
} from 'jnpl-helper';

export class Helper {
	env: string = process.env.NODE_ENV || 'local';

	constructor(private config) {}

	cleanSequelizeData(data: any): Object {
		return typeof data.get !== 'undefined' ? data.get({ plain: true }) : data;
	}

	cleanSequelizeDataArray(data: any): Array<any> {
		return data.map(this.cleanSequelizeData);
	}

	toJson(jsonData: any = ''): any {
		return toJson(jsonData);
	}

	toString(jsonData: any = ''): any {
		return toString(jsonData);
	}

	cleanData(data: any = ''): any {
		return cleanData(data);
	}

	cleanDataWithNull(data: any = ''): any {
		return cleanDataWithNull(data);
	}

	cleanDataRemoveNull(data: any = ''): any {
		return cleanDataRemoveNull(data);
	}

	isEmptyObject(obj: Object = {}): boolean {
		return isEmptyObject(obj);
	}

	stringExist(string: string = '', character: string = ''): boolean {
		return isStringExist(string, character);
	}

	isNotEmpty(v: any = null): boolean {
		return isNotEmpty(v);
	}

	isEmpty(v: any = null): boolean {
		return isEmpty(v);
	}

	validateData(obj: Object = {}, list: Array<any> = []): boolean {
		return checkObjectInList(obj, list);
	}

	isInteger(num: number = 0): boolean {
		return isInteger(num);
	}

	isInArray(value: any = '', array: Array<any> = []): boolean {
		return isInArray(value, array);
	}

	pad(data: number = 0): string {
		return pad(data);
	}

	get secretKey(): string {
		return this.config.secretKey || '';
	}

	currentDate(): string {
		const dateObj = new Date();
		return dateObj.getFullYear() + '-' + pad(dateObj.getMonth() + 1) + '-' + pad(dateObj.getDate()) + ' 23:59:59';
	}

	subtractDate(days: number): string {
		const dateObj = new Date();
		dateObj.setDate(dateObj.getDate() - days);
		return dateObj.getFullYear() + '-' + pad(dateObj.getMonth() + 1) + '-' + pad(dateObj.getDate()) + ' 00:00:00';
	}
}
